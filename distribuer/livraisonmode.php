<?php

/**
 * Abonner un auteur
 *
 * @plugin     Abonnements
 * @copyright  2014
 * @author     cedric
 * @licence    GNU/GPL
 * @package    SPIP\Abos\API
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Distribuer un mode de livraison = creer le bordereaulivraison correspondant
 * @param $id_livraisonmode
 * @param $detail
 * @param $commande
 * @return bool|string
 */
function distribuer_livraisonmode_dist($id_livraisonmode, $detail, $commande) {

	if ($detail['statut'] == 'attente') {
		$detailprix = json_decode($detail['infos_extras'], true);

		if ($detailprix and !empty($detailprix['id_details'])) {
			include_spip('action/editer_objet');
			$set = array(
				'id_livraisonmode' => $id_livraisonmode,
				'id_commande' => $commande['id_commande'],
				'details' => json_encode($detailprix['id_details'])
			);
			if ($id_bordereaulivraison = objet_inserer('bordereaulivraison',null, $set)) {
				return 'envoye';
			}
		}
	}

	return false;
}
