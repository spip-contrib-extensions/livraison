<?php
/**
 * Fonctions utiles au plugin Livraison
 *
 * @plugin     Livraison
 * @copyright  2015
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Livraison\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Génère un numéro unique utilisé pour remplir le champ `reference` lors de la création d'un bon de livraison.
 *
 * Le numéro retourné est le nombre de secondes écoulées depuis le 1er janvier 1970
 *
 * @example
 *     ```
 *     $fonction_reference = charger_fonction('bordereaulivraison_reference', 'inc/');
 *     ```
 *
 * @param int $id_commande
 * @param int $id_auteur
 *     (optionnel) identifiant de l'auteur
 * @return string
 *     reference de la commande
**/
function inc_bordereaulivraison_reference_dist($id_bordereaulivraison) {

	$t = $_SERVER['REQUEST_TIME'];
	if ($id_bordereaulivraison = intval($id_bordereaulivraison)) {
		if ($date = sql_getfetsel('date', 'spip_bordereaulivraisons', 'id_commande=' . intval($id_bordereaulivraison))) {
			$t = strtotime($date);
		}
	}
	else {
		$id_bordereaulivraison = 'xxxxxx';
	}

	// format yymmddHHiiNNNNNN
	$reference = "L" . date('ymdHi', $t) . str_pad($id_bordereaulivraison, 6, '0', STR_PAD_LEFT);

	return $reference;
}