<?php
/**
 * Fonctions utiles au plugin Livraison
 *
 * @plugin     Livraison
 * @copyright  2015
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Livraison\inc\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Passer les code_pays en code alpha 3 si besoin
 * @param $code_pays
 * @return mixed|string
 */
function livraison_normalise_code_pays($code_pays) {
	static $code_conversion;
	$code_pays = trim($code_pays);
	if (strlen($code_pays) == 2 and test_plugin_actif('pays')) {
		if (is_null($code_conversion)) {
			$code_conversion = sql_allfetsel('code, code_alpha3', 'spip_pays');
			$code_conversion = array_column($code_conversion, 'code_alpha3', 'code');
		}
		if (isset($code_conversion[$code_pays])) {
			$code_pays = $code_conversion[$code_pays];
		}
	}
	return $code_pays;
}

/**
 * Transformer le tableau texte du bareme poids/prix ou volume/prix
 * en tableau PHP
 * @param $bareme_str
 * @return array|bool
 */
function livraison_poids_volume_bareme_from_string($bareme_str){
	if (!strlen(trim($bareme_str))){
		return false;
	}
	$t = explode("\n",$bareme_str);
	$bareme = array();
	foreach($t as $i){
		$i = explode("|",$i);
		$unite = trim(reset($i));
		$cout = trim(end($i));
		$bareme[$unite] = (($cout==="NA" OR $cout==="N/A")?false:floatval(trim(end($i))));
	}
	return $bareme;
}

/**
 * Calculer le cout en fonction de la mesure (poids, volume) en appliquant le bareme fourni
 * @param $mesure
 * @param $bareme
 * @return bool
 */
function livraison_appliquer_bareme($mesure,$bareme){

	foreach($bareme as $limite=>$prix){
		if ($mesure<floatval($limite)) {
			return $prix;
		}
	}

	// on est sorti du bareme, on ne peut pas l'appliquer
	return false;
}


/**
 * Calculer le cout de livraison
 * @param int|array $id_commande_ou_details
 * @param int $id_livraisonmode
 * @param string $pays
 * @param string $code_postal
 * @param null|array $partiel
 * @return array|bool
 */
function livraison_calculer_cout($id_commande_ou_details,$id_livraisonmode,$pays,$code_postal, $partiel = null){
	$id_non_livres = array();
	$pays = livraison_normalise_code_pays($pays);

	if (!$id_livraisonmode
	  OR !$mode = sql_fetsel("*","spip_livraisonmodes","id_livraisonmode=".intval($id_livraisonmode))){
		return false;
	}

	// verifier si le pays est dans la zone_pays eventuelle
	if (strlen($mode['zone_pays'])){
		$zone_pays = explode(',',$mode['zone_pays']);
		$zone_pays = array_map('livraison_normalise_code_pays',$zone_pays);
		if (!in_array($pays,$zone_pays)){
			return false;
		}
	}

	// verifier que le pays n'est pas dans la zone_pays_exclus eventuelle
	if (strlen($mode['zone_pays_exclus'])){
		$zone_pays_exclus = explode(',',$mode['zone_pays_exclus']);
		$zone_pays_exclus = array_map('livraison_normalise_code_pays',$zone_pays_exclus);
		if (in_array($pays,$zone_pays_exclus)){
			return false;
		}
	}

	// verifier si le CP est dans la zone_cp eventuelle
	if (strlen($mode['zone_cp'])){
		$zone_cp = explode(',',$mode['zone_cp']);
		$zone_cp = array_map('trim',$zone_cp);
		$ok = false;
		foreach($zone_cp as $cp_ok){
			if (strncmp($code_postal,$cp_ok,strlen($cp_ok))==0){
				$ok = true;
				continue;
			}
		}
		if (!$ok) {
			return false;
		}
	}

	// verifier si le CP est dans la zone_cp_exclus eventuelle
	if (strlen($mode['zone_cp_exclus'])){
		$zone_cp_exclus = explode(',',$mode['zone_cp_exclus']);
		$zone_cp_exclus = array_map('trim',$zone_cp_exclus);
		foreach($zone_cp_exclus as $cp_exclus){
			if (strncmp($code_postal,$cp_exclus,strlen($cp_exclus))==0){
				return false;
			}
		}
	}

	// OK on est dans le cas ou le mode s'applique a cette adresse
	if (is_array($id_commande_ou_details)) {
		// Filtrer les details en ne prenant que le partiel si fourni, et en enlevant les livraisonmode
		$details = $id_commande_ou_details;
		foreach ($details as $k => $detail) {
			if ($detail['objet'] === 'livraisonmode') {
				unset($details[$k]);
			}
			elseif (!is_null($partiel) and !in_array($detail['id_detail'], $partiel)) {
				unset($details[$k]);
			}
		}
	}
	else {
		$where_partiel = '';
		if (!is_null($partiel)) {
			$where_partiel = " AND ".sql_in('id_commandes_detail', $partiel);
		}
		$details = sql_allfetsel(
			"*, id_commandes_detail AS id_detail",
			"spip_commandes_details",
			"id_commande=".intval($id_commande_ou_details)." AND objet<>".sql_quote('livraisonmode') . $where_partiel);
	}

	$prix = 0;
	$taxe = 0;

	// verifier que le mode est applicable a toutes les lignes de la commande
	// ou au moins a certaines lignes si on accepte un mode de livraison partiel
	$partiellement_applicable = false;
	foreach($details as $k => $detail){
		// si on a fourni une liste $partiel des details a livrer,
		// on accepte une livraison partielle en renvoyant la liste des id non livres
		if (!livraison_applicable($detail['objet'],$detail['id_objet'],$id_livraisonmode)) {
			if ($partiel) {
				$id_non_livres[] = $detail['id_detail'];
				unset($details[$k]);
			}
			else {
				return false;
			}
		}
		else {
			$partiellement_applicable = true;
		}
	}
	// si le mode ne s'applique a aucune ligne, on arrete la
	if (!$partiellement_applicable){
		return false;
	}

	if (strlen($mode['taxe'])) {
		$taxe = floatval($mode['taxe']);
	}

	// le prix forfaitaire initial
	if (strlen($mode["prix_forfait_ht"])){
		$prix += floatval($mode["prix_forfait_ht"]);
	}

	// le prix en fonction du nombre de produits
	if (strlen($mode["prix_unit_ht"])){
		$prix += count($details) * floatval($mode["prix_unit_ht"]);
	}

	// le prix en fonction du poids et/ou du volume
	$bareme_poids = livraison_poids_volume_bareme_from_string($mode['prix_poids_ht']);
	$bareme_volume = livraison_poids_volume_bareme_from_string($mode['prix_volume_ht']);

	$poids_total = $volume_total = 0;
	if ($bareme_poids OR $bareme_volume){
		foreach($details as $detail){
			if ($mesure = charger_fonction($detail['objet'],"mesure",true)
			  OR $mesure = charger_fonction("defaut","mesure",true)){
				list($poids,$volume) = $mesure($detail['objet'],$detail['id_objet'],$detail['quantite']);
				$poids_total += $poids;
				$volume_total += $volume;
			}
		}
		if ($poids_total>0 AND $bareme_poids){
			$p = livraison_appliquer_bareme($poids_total,$bareme_poids);
			// si on est hors bareme, on ne peut pas utiliser ce mode de livraison
			// TODO : si le poids depasse le poids maxi, couper le colis en 2,3... pour calculer le prix
			if ($p===false) {
				return false;
			}
			$prix += $p;
		}
		if ($volume_total>0 AND $bareme_volume){
			$p = livraison_appliquer_bareme($volume_total,$bareme_volume);
			// si on est hors bareme, on ne peut pas utiliser ce mode de livraison
			// TODO : si le volume depasse le volume maxi, couper le colis en 2,3... pour calculer le prix
			if ($p===false) {
				return false;
			}
			$prix += $p;
		}
	}

	$prix = round($prix,2);

	return array($prix, $taxe, $id_non_livres);
}

/**
 * Verifier qu'un mode est applicable a un objet de la commande
 * @param string $objet
 * @param int $id_objet
 * @param int $id_livraisonmode
 * @return bool
 */
function livraison_applicable($objet, $id_objet, $id_livraisonmode) {
	// si l'objet n'a pas besoin de livraison c'est OK pour ce mode de livraison
	if (!objet_livraison_necessaire($objet, $id_objet)) {
		return true;
	}

	$modespossibles = sql_allfetsel('id_livraisonmode','spip_livraisonmodes_liens','objet='.sql_quote($objet).' AND id_objet='.sql_quote($id_objet));
	// si aucun mode associe a l'objet, tous les modes sont possibles, donc OK
	if (count($modespossibles)) {
		$modespossibles = array_column($modespossibles, 'id_livraisonmode');
		if (!in_array($id_livraisonmode, $modespossibles)) {
			return false;
		}
	}
	return true;
}

/**
 * Supprimer le mode de livraison d'une commande
 * @param $id_commande
 */
function reset_livraison_commande($id_commande){
	$where = "id_commande=".intval($id_commande)." AND objet=".sql_quote('livraisonmode');
	sql_delete("spip_commandes_details",$where);
}

/**
 * Déterminer si un objet précis nécessite une livraison ou non
 * @param string $objet
 * @param int $id_objet
 * @param ?array $row
 * @return bool
 */
function objet_livraison_necessaire($objet, $id_objet, $row = null) {
	static $objets_connus = array();
	// si c'est une categorie d'objet pour lequel on sait car cela ne dépend pas de chaque objet, c'est réglé
	if (isset($objets_connus[$objet])) {
		return $objets_connus[$objet];
	}

	if (is_null($row)) {
		$table = table_objet_sql($objet);
		$primary = id_table_objet($objet);
		$row = sql_fetsel("*",$table,"$primary=".intval($id_objet));
	}

	if (isset($row['immateriel'])) {
		// si il y a une colonne immateriel sur cet objet, c'est au cas par cas, donc on ne memoize pas
		return $row['immateriel'] ? false : true;
	}
	else {
		// sinon, cela dépends des colonnes ou de la précense d'une fonction de mesure
		// et dans ce cas c'est toujours pareil pour tous les objets de même type, donc on memoize
		// est-ce un objet avec poids et volume définissables ?
		// si oui il necessite une livraison par défaut
		if (isset($row['poids'])) {
			if (isset($row['volume'])){
				return $objets_connus[$objet] = true;
			}
			if(isset($row['longueur']) and isset($row['largeur']) and isset($row['hauteur'])){
				return $objets_connus[$objet] = true;
			}
		}
		// sinon, si il y a une fonction mesure definie pour ces objets, alors ils nécessitent bien une livraison
		if (charger_fonction($objet,"mesure",true)) {
			return $objets_connus[$objet] = true;
		}
		return $objets_connus[$objet] = false;
	}
}

/**
 * Verifier si une commande necessite une livraison ou pas
 * @param $id_commande
 * @return bool
 */
function commande_livraison_necessaire($id_commande){
	static $livrable = array();
	if (isset($livrable[$id_commande])){
		return $livrable[$id_commande];
	}
	$items = sql_allfetsel("*","spip_commandes_details","id_commande=".intval($id_commande));
	$livrable[$id_commande] = false;
	foreach($items as $item){
		if (objet_livraison_necessaire($item['objet'], $item['id_objet'])) {
			$livrable[$id_commande] = true;
			break;
		}
	}
	return $livrable[$id_commande];
}


/**
 * Construire la liste des choix de livraison possible, si necessaire en combinant plusieurs livraisons
 *
 * @param int|array $id_commande_ou_details
 * @param string $pays
 * @param string $code_postal
 * @param null|array $partiel
 * @param int $from_id_livraisonmode
 * @return array
 */
function commande_trouver_livraisons_possibles($id_commande_ou_details, $pays, $code_postal, $partiel = null, $from_id_livraisonmode = 0){

	$choix_livraison = array();

	// tous les modes de livraison disponibles
	$id_livraisonmodes = sql_allfetsel("id_livraisonmode", "spip_livraisonmodes", "statut=" . sql_quote('publie') . ' AND id_livraisonmode>' . intval($from_id_livraisonmode), '', 'id_livraisonmode');
	$id_livraisonmodes = array_column($id_livraisonmodes, 'id_livraisonmode');

	// lister les details a livrer (qui seront dans la decomposition)
	if (is_array($id_commande_ou_details)) {
		$id_details = array_column($id_commande_ou_details, 'id_detail');
	}
	else {
		$id_commande = intval($id_commande_ou_details);
		$where_partiel = '';
		if (!is_null($partiel)){
			$where_partiel = " AND " . sql_in('id_commandes_detail', $partiel);
		}
		$id_details = sql_allfetsel("id_commandes_detail as id_detail", "spip_commandes_details", "id_commande=" . intval($id_commande) . " AND objet<>" . sql_quote('livraisonmode') . $where_partiel);
		$id_details = array_column($id_details, 'id_detail');
	}

	// d'abord on cherche des modes de livraison qui permettent de tout livrer d'un coup
	// uniquement si pas sous ensemble $partiel fourni !
	if (is_null($partiel)){
		foreach ($id_livraisonmodes as $id_livraisonmode){
			if ($cout = livraison_calculer_cout($id_commande_ou_details, $id_livraisonmode, $pays, $code_postal)){
				list($prix_ht, $taxe) = $cout;
				$prix_ttc = round($prix_ht+$prix_ht*$taxe, 2);
				$choix_livraison[$id_livraisonmode] = array(
					'id' => $id_livraisonmode,
					'prix_ht' => $prix_ht,
					'prix_ttc' => $prix_ttc,
					'decomposition' => array(
						$id_livraisonmode => array(
							'prix_ht' => $prix_ht,
							'taxe' => $taxe,
							'prix_ttc' => $prix_ttc,
							'id_details' => $id_details,
						)
					)
				);
			}
		}

		// si on a trouve au moins un mode de livraison globale on arrete la
		if (count($choix_livraison)){
			return $choix_livraison;
		}
	}

	foreach ($id_livraisonmodes as $id_livraisonmode){
		if ($cout = livraison_calculer_cout($id_commande_ou_details, $id_livraisonmode, $pays, $code_postal, $id_details)){
			list($prix_ht, $taxe, $id_non_livres) = $cout;
			// chercher d'autres modes de livraison pour le restant
			if ($id_non_livres){
				$id_details_livres = array_diff($id_details, $id_non_livres);
				$sous_choix = commande_trouver_livraisons_possibles($id_commande_ou_details, $pays, $code_postal, $id_non_livres, $id_livraisonmode);
				if (count($sous_choix)){
					foreach ($sous_choix as $sous){
						$id = explode(',', $sous['id']);
						array_unshift($id, $id_livraisonmode);
						$id = implode(',', $id);
						$prix_ttc = round($prix_ht+$prix_ht*$taxe, 2);
						$decomposition = array();
						$decomposition[$id_livraisonmode] = array(
							'prix_ht' => $prix_ht,
							'taxe' => $taxe,
							'prix_ttc' => $prix_ttc,
							'id_details' => $id_details_livres,
						);
						foreach ($sous['decomposition'] as $k=>$s) {
							$decomposition[$k] = $s;
						}
						$choix_livraison[$id] = array(
							'id' => $id,
							'prix_ht' => $sous['prix_ht']+$prix_ht,
							'prix_ttc' => $sous['prix_ttc']+$prix_ttc,
							'decomposition' => $decomposition,
						);
					}
				}
			} // on arrive a livrer tout le restant d'un coup !
			else {
				$prix_ttc = round($prix_ht+$prix_ht*$taxe, 2);
				$choix_livraison[$id_livraisonmode] = array(
					'id' => $id_livraisonmode,
					'prix_ht' => $prix_ht,
					'prix_ttc' => $prix_ttc,
					'decomposition' => array(
						$id_livraisonmode => array(
							'prix_ht' => $prix_ht,
							'taxe' => $taxe,
							'prix_ttc' => $prix_ttc,
							'id_details' => $id_details,
						)
					)
				);
			}
		}
	}

	return $choix_livraison;
}

/**
 * Déterminer le CP et pays à utiliser pour le calcul de la livraison
 * @param int $id_commande
 * @param array $options
 *   string $code_postal : pour prendre en compte des coordonnes de livraison spécifiques
 *   string $pays : pour prendre en compte des coordonnes de livraison spécifiques
 * @return array
 */
function livraison_commande_choisir_cp_pays($id_commande, $options = []) {
	// SI on fournit un code_pays ET un code_postal, ils sont pris comme reférence
	if (
		!empty($options['pays'])
		and !empty($options['code_postal'])
	) {
		$pays = $options['pays'];
		$cp = $options['code_postal'];
	}
	// sinon on cherche dans la infos client de la commande
	else {
		$pays = $cp = null;
		$commande_infos_client = charger_fonction('infos_client', 'commande');
		$infos = $commande_infos_client($id_commande);
		if (!empty($infos['livraison']['adresse'])) {
			$pays = (empty($infos['livraison']['adresse']['pays']) ? '' : $infos['livraison']['adresse']['pays']);
			$cp = (empty($infos['livraison']['adresse']['code_postal']) ? '' : $infos['livraison']['adresse']['code_postal']);
		}
	}

	return [$cp, $pays];
}

/**
 * Ajouter/mettre a jour le mode et le cout de livraison de la commande
 * @param int $id_commande
 * @param int $id_livraisonmode
 *   si pas fourni on reprend celui deja existant pour une mise a jour du cout
 * @param array $options
 *   float $reduction: coeff de reduction à appliquer sur les frais de livraison (entre 0 et 1 donc)
 *   string $code_postal : pour prendre en compte des coordonnes de livraison spécifiques
 *   string $pays : pour prendre en compte des coordonnes de livraison spécifiques
 * @return bool
 */
function fixer_livraison_commande($id_commande, $id_livraisonmode=0, $options = []){
	$where = "id_commande=".intval($id_commande)." AND objet=".sql_quote('livraisonmode');
	if (is_scalar($options)) {
		$options = [
			'reduction' => $options
		];
	}
	$reduction = (isset($options['reduction']) ? $options['reduction'] : 0);

	if (!$id_commande
	  OR !$commande = sql_fetsel("*","spip_commandes","id_commande=".intval($id_commande))){
		return false;
	}

	if (!commande_livraison_necessaire($id_commande)) {
		// enlever les modes de livraison dont on a pas besoin et rien d'autre à faire
		sql_delete("spip_commandes_details",$where);
		return true;
	}

	[$cp, $pays] = livraison_commande_choisir_cp_pays($id_commande, $options);

	// si on ne connait pas le pays
	if (empty($pays) or empty($cp)) {
		return false;
	}

	$choix_livraison = commande_trouver_livraisons_possibles($id_commande, $pays, $cp);

	if (!$id_livraisonmode and count($choix_livraison) == 1) {
		$id_livraisonmode = array_keys($choix_livraison);
		$id_livraisonmode = reset($id_livraisonmode);
	}

	// pas de mode de livraison applicable ou incorrect : on sort de la
	// en faisant reset sur la livraison de la commande
	if (!$id_livraisonmode or !isset($choix_livraison[$id_livraisonmode])){
		sql_delete("spip_commandes_details",$where);
		return false;
	}

	$livraison = $choix_livraison[$id_livraisonmode];
	$id_livraisonmodes = array_keys($livraison['decomposition']);

	// enlever les modes de livraison dont on a pas besoin
	sql_delete("spip_commandes_details",$where . ' AND '.sql_in('id_objet', $id_livraisonmodes, 'NOT'));

	foreach($livraison['decomposition'] as $id_livraisonmode => $detailprix) {
		$mode = sql_getfetsel("titre","spip_livraisonmodes","id_livraisonmode=".intval($id_livraisonmode));
		include_spip('inc/texte');
		$mode = typo($mode);
		$id_commandes_detail = sql_getfetsel("id_commandes_detail","spip_commandes_details",$where . ' AND id_objet='.intval($id_livraisonmode),'');
		$set = array(
			'id_commande' => $id_commande,
			'descriptif' => _T('livraison:info_livraison_par',array('mode'=>$mode)),
			'quantite' => 1,
			'prix_unitaire_ht' => $detailprix['prix_ht'],
			'taxe' => $detailprix['taxe'],
			'reduction' => $reduction,
			'objet' => 'livraisonmode',
			'id_objet' => $id_livraisonmode,
			'infos_extras' => json_encode($detailprix),
			'statut' => 'attente',
		);
		if (!$id_commandes_detail) {
			sql_insertq("spip_commandes_details",$set);
		}
		else {
			sql_updateq("spip_commandes_details",$set,'id_commandes_detail='.intval($id_commandes_detail));
		}
	}

	return true;
}


function livraison_envoyee_repercuter_commande($id_bordereaulivraison) {
	$bordereau = sql_fetsel('*', 'spip_bordereaulivraisons', 'id_bordereaulivraison='.intval($id_bordereaulivraison));
	if ($bordereau and in_array($bordereau['statut'], array('envoye', 'livre'))) {
		$id_commande = $bordereau['id_commande'];
		$details = json_decode($bordereau['details'], true);

		sql_updateq('spip_commandes_details', ['statut' => 'envoye'], "id_commande=".intval($id_commande). " AND ".sql_in('id_commandes_detail', $details));
		// il faut aussi mettre en envoye la livraison concernee au cas ou
		sql_updateq('spip_commandes_details', ['statut' => 'envoye'], "id_commande=".intval($id_commande). " AND objet='livraisonmode' AND id_objet=".intval($bordereau['id_livraisonmode']));

		// et on actualise le statut de la commande
		include_spip('inc/commandes');
		commandes_actualiser_statut_si_commande_envoyee($id_commande);
	}
}

function livraison_retournee_repercuter_commande($id_bordereaulivraison) {
	$bordereau = sql_fetsel('*', 'spip_bordereaulivraisons', 'id_bordereaulivraison='.intval($id_bordereaulivraison));
	if ($bordereau and $bordereau['statut'] === 'retourne') {
		$id_commande = $bordereau['id_commande'];
		$details = json_decode($bordereau['details'], true);

		sql_updateq('spip_commandes_details', ['statut' => 'retour'], "id_commande=".intval($id_commande). " AND ".sql_in('id_commandes_detail', $details));
		// il faut aussi mettre en retour la livraison concernee
		sql_updateq('spip_commandes_details', ['statut' => 'retour'], "id_commande=".intval($id_commande). " AND objet='livraisonmode' AND id_objet=".intval($bordereau['id_livraisonmode']));

		// et on actualise le statut de la commande
		include_spip('inc/commandes');
		commandes_actualiser_statut_si_commande_envoyee($id_commande);
	}
}