<?php

/**
 * Imprimer un bordereaulivraison
 *
 * @plugin     Livraison
 * @copyright  2015
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Livraison\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


include_spip('base/abstract_sql');
function action_imprimer_bordereaulivraison_dist($arg = null) {

	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	$id_bordereaulivraison = intval($arg);
	include_spip('inc/autoriser');
	if (autoriser('voir', 'bordereaulivraison', $id_bordereaulivraison)) {

		$html = recuperer_fond('prive/squelettes/inclure/bordereaulivraison', array('id_bordereaulivraison' => $id_bordereaulivraison));
		include_spip('inc/actions');
		ajax_retour($html, 'text/html');
		exit;
	}

}
