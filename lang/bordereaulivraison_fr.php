<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_bordereaulivraison' => 'Ajouter ce Bon de livraison',

	// C
	'champ_descriptif_label' => 'Descriptif',
	'champ_reference_label' => 'Référence',
	'champ_details_label' => 'Détails',
	'champ_id_commande_label' => 'Commande',
	'champ_id_commande_short' => 'Cde',
	'champ_id_livraisonmode_label' => 'Mode de livraison',
	'champ_id_livraisonmode_short' => 'Mode',
	'champ_date_envoi_label' => 'Date Envoi',
	'lang/bordereaulivraison_fr.php' => 'Contenu de la livraison',

	// I
	'icone_creer_bordereaulivraison' => 'Créer un Bon de livraison',
	'icone_modifier_bordereaulivraison' => 'Modifier ce Bon de livraison',
	'info_1_bordereaulivraison' => 'Un Bon de livraison',
	'info_aucun_bordereaulivraison' => 'Aucun Bon de livraison',
	'info_bordereaulivraisons_auteur' => 'Les modes de livraison de cet auteur',
	'info_nb_bordereaulivraisons' => '@nb@ modes de livraison',

	'info_bordereaulivraison_date_du' => 'du',

	// R
	'retirer_lien_bordereaulivraison' => 'Retirer ce Bon de livraison',
	'retirer_tous_liens_bordereaulivraisons' => 'Retirer tous les modes de livraison',

	// T
	'texte_ajouter_bordereaulivraison' => 'Ajouter un Bon de livraison',
	'texte_changer_statut_bordereaulivraison' => 'Cette livraison est :',
	'texte_creer_associer_bordereaulivraison' => 'Créer et associer un Bon de livraison',
	'texte_definir_comme_traduction_bordereaulivraison' => 'Ce Bon de livraison est une traduction du Bon de livraison numéro :',
	'titre_langue_bordereaulivraison' => 'Langue de ce Bon de livraison',
	'titre_bordereaulivraison' => 'Bon de livraison',
	'titre_bordereaulivraisons' => 'Bons de livraison',
	'titre_logo_bordereaulivraison' => 'Logo de ce Bon de livraison',

	'statut_prepa' => 'En préparation',
	'statut_prop' => 'Préparé, à envoyer',
	'statut_envoye' => 'Envoyé',
	'statut_livre' => 'Livré',
	'statut_retourne' => 'Retourné',
	'statut_abandonne' => 'Abandonné',
	'statut_poubelle' => 'Poubelle',

	'texte_statut_prepa' => 'en préparation',
	'texte_statut_prop' => 'prête à envoyer',
	'texte_statut_envoye' => 'envoyée',
	'texte_statut_livre' => 'livrée',
	'texte_statut_retourne' => 'retournée',
	'texte_statut_abandonne' => 'abandonnée',
	'texte_statut_poubelle' => 'poubelle',

	'erreur_commande_inexistante' => 'Cette commande n\'existe pas',
	'erreur_commande_non_livrable' => 'Cette commande n\'est pas livrable',
	'erreur_livraisonmode_non_disponible' => 'Ce mode de livraison n\'est pas disponible',

	'info_rien_a_preparer' => 'Rien à envoyer',
	'info_rien_a_envoyer' => 'Rien à envoyer',

	'titre_livraisons_a_preparer' => 'Livraisons à préparer',
	'titre_livraisons_a_envoyer' => 'Livraisons à envoyer',
	'titre_livraisons_envoyees' => 'Livraisons envoyées',
	'titre_livraisons_livrees' => 'Livraisons livrées',
	'titre_livraisons_en_echec' => 'Livraisons en echec',

	'info_bordereaulivraison_vide' => 'Cette livraison est vide',
	'abbr_dans_la_livraison' => 'inclus',

	'label_total_quantite' => 'Nombre total d\'objets',

);
