# Livraison — Changelog

## Unreleased

- #20 éviter un warning lorsque `$flux['args']['action']` n'est pas défini (cf. spip/spip#6049)
