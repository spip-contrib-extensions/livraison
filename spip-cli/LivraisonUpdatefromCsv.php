<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class LivraisonUpdatefromCsv extends Command {
	protected function configure() {
		$this
			->setName('livraison:updatefrom:csv')
			->setDescription('Mettre à jour les tarifs de livraison depuis un CSV')
			->addOption(
				'fichier',
				null,
				InputOption::VALUE_REQUIRED,
				'chemin vers le fichier CSV',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$this->io->title("Mise à jour des modes de livraison");

		$fichier = $input->getOption('fichier');
		if (!$fichier or !file_exists($fichier)) {
			$this->io->error("Indiquer un chemin vers un fichier CSV");
			return self::FAILURE;
		}

		$options = 	[
			'head' => true,
			'delim' => ',',
		];

		$importer_csv = charger_fonction('importer_csv', 'inc');
		$modes = $importer_csv($fichier, $options);

		if (empty($modes)) {
			$this->io->error("Aucun mode de livraison à mettre à jour dans le fichier $fichier");
			return self::FAILURE;
		}

		// changer les retour ligne par des espaces dans les tableaux de prix au poids et volume, pour simplifier le CSV
		foreach ($modes as &$mode) {
			foreach (['prix_poids_ht', 'prix_volume_ht'] as $k) {
				$mode[$k] = preg_replace(",\s+,","\n",$mode[$k]);
			}
		}


		$deja = [];
		foreach ($modes as $nb_ligne => $mode) {
			$dest_id_livraisonmode = 'new';
			if (isset($mode['id_livraisonmode'])) {
				// si il y a une colonne id_livraisonmode dans le fichier, on l'utilise pour faire matcher les modes importés
				if (strlen($mode['id_livraisonmode']) and intval($mode['id_livraisonmode'])) {
					$dest_id_livraisonmode = $mode['id_livraisonmode'];
				}
			}
			else {
				// sinon on cherche avec le titre
				if ($id = sql_getfetsel('id_livraisonmode', 'spip_livraisonmodes', 'titre='.sql_quote($mode['titre'])." AND ".sql_in('id_livraisonmode', $deja, 'NOT'))) {
					$dest_id_livraisonmode = $id;
				}
			}

			$titre = "Ligne ".($nb_ligne+1)." : " . $mode['titre'];
			if (intval($dest_id_livraisonmode)) {
				if ($row = sql_fetsel('titre', 'spip_livraisonmodes', 'id_livraisonmode='.intval($dest_id_livraisonmode))) {
					$this->io->care($titre ." => import dans #$dest_id_livraisonmode ". $row['titre']);
				}
				else {
					$dest_id_livraisonmode = 'new';
				}
			}
			if (!intval($dest_id_livraisonmode)) {
				$this->io->care($titre ." => Import dans un nouveau mode de livraison");
			}
			$confirm = $this->io->confirm("Confirmez-vous ?", false);
			if ($confirm) {
				$id = self::importerMode($dest_id_livraisonmode, $mode);
				if (!$id) {
					$this->io->error('Echec import');
					return self::FAILURE;
				}
				$this->io->check("Import dans #$id");
			}
		}

		return self::SUCCESS;

	}

	protected static function importerMode($dest_id_livraisonmode, $mode) {
		include_spip('action/editer_objet');
		include_spip('inc/autoriser');

		if (isset($mode['id_livraisonmode'])) {
			unset($mode['id_livraisonmode']);
		}

		if (!intval($dest_id_livraisonmode)) {
			$dest_id_livraisonmode = objet_inserer('livraisonmode', null);
			if (!$dest_id_livraisonmode) {
				return false;
			}
			$mode['statut'] = 'prop';
		}
		autoriser_exception("modifier", "livraisonmode", $dest_id_livraisonmode);
		autoriser_exception("instituer", "livraisonmode", $dest_id_livraisonmode);
		objet_modifier('livraisonmode', $dest_id_livraisonmode, $mode);
		autoriser_exception("modifier", "livraisonmode", $dest_id_livraisonmode, false);
		autoriser_exception("instituer", "livraisonmode", $dest_id_livraisonmode, false);

		return $dest_id_livraisonmode;
	}
}
