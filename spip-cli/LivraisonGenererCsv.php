<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;


class LivraisonGenererCsv extends Command {
	protected function configure() {
		$this
			->setName('livraison:generer:csv')
			->setDescription('Générer un nouveau CSV d\'installation des modes de livraison à partir des modes en base')
			->addOption(
				'id_livraisonmode',
				null,
				InputOption::VALUE_REQUIRED,
				'selections de mode à utiliser pour générer le CSV',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$id_livraisonmode = $input->getOption('id_livraisonmode');
		if ($id_livraisonmode) {
			$id_livraisonmode = explode(',', $id_livraisonmode);
			$id_livraisonmode = array_map('intval', $id_livraisonmode);
		}

		$this->io->title("Exporter les modes de livraison");

		$modes = sql_allfetsel(
			"titre,descriptif,zone_pays,zone_cp,zone_cp_exclus,taxe,prix_forfait_ht,prix_unit_ht,prix_poids_ht,prix_volume_ht",
			'spip_livraisonmodes',
			$id_livraisonmode ? sql_in('id_livraisonmode', $id_livraisonmode) : ''
		);

		if (!empty($modes)) {
			// changer les retour ligne par des espaces dans les tableaux de prix au poids et volume, pour simplifier le CSV
			foreach ($modes as &$mode) {
				foreach (['prix_poids_ht', 'prix_volume_ht'] as $k) {
					$mode[$k] = str_replace("\r\n", "\n", $mode[$k]);
					$mode[$k] = str_replace("\r", "\n", $mode[$k]);
					$mode[$k] = str_replace("\n", " ", $mode[$k]);
				}
			}

			$entetes = reset($modes);
			$entetes = array_keys($entetes);
			$options = [
				'delim' => ',',
				'entetes' => $entetes,
				'envoyer' => false,
				'charset' => 'utf-8'
			];
			$exporter_csv = charger_fonction('exporter_csv', 'inc');
			$fichier = $exporter_csv('livraisonsmode', $modes, $options);

			$this->io->check($fichier);
			return self::SUCCESS;
		}

		$this->io->error("Aucun mode de livraison à exporter");


		return self::FAILURE;

	}
}
