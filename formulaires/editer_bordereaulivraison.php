<?php
/**
 * Gestion du formulaire de d'édition de bordereaulivraison
 *
 * @plugin     Livraison
 * @copyright  2015
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Livraison\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_bordereaulivraison
 *     Identifiant du bordereaulivraison. 'new' pour un nouveau bordereaulivraison.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un bordereaulivraison source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du bordereaulivraison, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_bordereaulivraison_identifier_dist($id_bordereaulivraison='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	return serialize(array(intval($id_bordereaulivraison)));
}

/**
 * Chargement du formulaire d'édition de bordereaulivraison
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_bordereaulivraison
 *     Identifiant du bordereaulivraison. 'new' pour un nouveau bordereaulivraison.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un bordereaulivraison source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du bordereaulivraison, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_bordereaulivraison_charger_dist($id_bordereaulivraison='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$valeurs = formulaires_editer_objet_charger('bordereaulivraison',$id_bordereaulivraison,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	if (!intval($id_bordereaulivraison)) {
		$bordereaulivraison_reference = charger_fonction('bordereaulivraison_reference', 'inc');
		$valeurs['reference'] = $bordereaulivraison_reference(0);
	}
	if (is_string($valeurs['details'])) {
		include_spip('livraison_fonctions');
		$valeurs['details'] = livraison_lister_details_bordereau($id_bordereaulivraison, $valeurs['details']);
	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de bordereaulivraison
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_bordereaulivraison
 *     Identifiant du bordereaulivraison. 'new' pour un nouveau bordereaulivraison.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un bordereaulivraison source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du bordereaulivraison, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_bordereaulivraison_verifier_dist($id_bordereaulivraison='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$erreurs = formulaires_editer_objet_verifier('bordereaulivraison',$id_bordereaulivraison, array('id_commande', 'id_livraisonmode'));

	$id_commande = _request('id_commande');
	if (!intval($id_commande) or !$commande = sql_fetsel('*', 'spip_commandes', 'id_commande='.intval($id_commande))) {
		$erreurs['id_commande'] = _T('bordereaulivraison:erreur_commande_inexistante');
	}
	else {
		include_spip('inc/autoriser');
		if (!autoriser('livrer', 'commande', $id_commande)) {
			$erreurs['id_commande'] = _T('bordereaulivraison:erreur_commande_non_livrable');
		}
	}

	$id_livraisonmode = _request('id_livraisonmode');
	if (!intval($id_livraisonmode)
		or !$livraisonmode = sql_fetsel('*', 'spip_livraisonmodes', 'id_livraisonmode='.intval($id_livraisonmode))
		or in_array($livraisonmode['statut'], array('prepa', 'prop', 'poubelle'))) {
		$erreurs['id_livraisonmode'] = _T('bordereaulivraison:erreur_livraisonmode_non_disponible');
	}

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de bordereaulivraison
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_bordereaulivraison
 *     Identifiant du bordereaulivraison. 'new' pour un nouveau bordereaulivraison.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un bordereaulivraison source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du bordereaulivraison, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_bordereaulivraison_traiter_dist($id_bordereaulivraison='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
	$details = _request('details');
	if (!is_array($details) or !$details) {
		$details = [];
	}
	set_request('details', json_encode($details));
	$res = formulaires_editer_objet_traiter('bordereaulivraison',$id_bordereaulivraison,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
	$res['editable'] = false;
	return $res;
}


?>
