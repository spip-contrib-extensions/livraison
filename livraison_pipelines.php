<?php
/**
 * Utilisations de pipelines par Livraison
 *
 * @plugin     Livraison
 * @copyright  2015
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Livraison\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


function livraison_post_insertion(array $flux) : array {
	if ($flux['args']['table'] === 'spip_bordereaulivraisons'
		and $id_bordereaulivreaison = $flux['args']['id_objet']
		and empty($flux['data']['reference'])) {

		$bordereaulivraison_reference = charger_fonction('bordereaulivraison_reference', 'inc');
		$flux['data']['reference'] = $bordereaulivraison_reference($id_bordereaulivreaison);

		// ajouter une reference
		sql_updateq('spip_bordereaulivraisons', array('reference' => $flux['data']['reference']), 'id_bordereaulivraison='.intval($id_bordereaulivreaison));

	}

	return $flux;
}

function livraison_pre_edition(array $flux) : array {
	if ($flux['args']['table'] === 'spip_bordereaulivraisons'
		and $id_bordereaulivreaison = $flux['args']['id_objet']
	    and !empty($flux['data']['statut'])) {

		// mettre a jour les d'envoi ou de livraison selon les statuts
		if ($flux['data']['statut'] === 'envoye' and empty($flux['data']['date_envoi'])) {
			$flux['data']['date_envoi'] = date('Y-m-d H:i:s');
		}
		if ($flux['data']['statut'] === 'livre' and empty($flux['data']['date_livre'])) {
			$flux['data']['date_livre'] = date('Y-m-d H:i:s');
		}
	}

	return $flux;
}

function livraison_post_edition(array $flux) : array {
	if (($flux['args']['action'] ?? '') === 'instituer'	) {
		if ($flux['args']['table'] === 'spip_bordereaulivraisons'
			and $id_bordereaulivraison = $flux['args']['id_objet']
		    and !empty($flux['data']['statut'])) {

			// declencher les actions
			if ($flux['data']['statut'] === 'envoye'
				or $flux['data']['statut'] === 'livre') {
				include_spip('inc/livraison');
				livraison_envoyee_repercuter_commande($id_bordereaulivraison);
			}

			// declencher les actions
			if ($flux['data']['statut'] === 'retourne') {
				include_spip('inc/livraison');
				livraison_retournee_repercuter_commande($id_bordereaulivraison);
			}

			if ($flux['data']['statut'] === 'livre' and empty($flux['data']['date_livre'])) {
				$flux['data']['date_livre'] = date('Y-m-d H:i:s');
			}
		}

	}

	return $flux;
}

function livraison_afficher_contenu_objet($flux){

	$adresse = '';
	$complement = '';
	if ($flux['args']['type']=='commande'
	  AND $id_commande = $flux['args']['id_objet']){
		$adresse = recuperer_fond("prive/objets/contenu/commande-adresse_livraison",array('id_commande'=>$id_commande));
		$complement = recuperer_fond("prive/objets/liste/bordereaulivraisons",array('id_commande'=>$id_commande));
	}
	elseif ($flux['args']['type']=='bordereaulivraison'
	  and $id_bordereaulivraison = $flux['args']['id_objet']
	  and $id_commande = sql_getfetsel('id_commande', 'spip_bordereaulivraisons', 'id_bordereaulivraison='.intval($id_bordereaulivraison))){
		$adresse = recuperer_fond("prive/objets/contenu/commande-adresse_livraison",array('id_commande'=>$id_commande));
	}
	if ($adresse) {
		if ($p = strpos($flux['data'],"</table>")){
			$flux['data'] = substr_replace($flux['data'],$adresse,$p+8,0);
		}
		else {
			$flux['data'] .= $adresse;
		}
	}
	if ($complement) {
		$flux['data'] .= $complement;
	}
	return $flux;
}

/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function livraison_affiche_milieu($flux) {
	$texte = "";

	if ($e = trouver_objet_exec($flux['args']['exec'])) {

		// livres sur les produits et offres abonnement
		if (!$e['edition']
		  and $objet = $e['type']
		  and $primary = $e['id_table_objet']
		  and $id_objet = intval($flux['args'][$primary])) {
			// si l'objet nécessite une livraison proposer le choix des modes de livraison
			include_spip('inc/livraison');
			if (objet_livraison_necessaire($objet, $id_objet)) {
				$flux['data'] .= recuperer_fond('prive/squelettes/inclure/liens-livraisonmodes', array(
					'table_source' => 'livraisonmodes',
					'objet' => $objet,
					'id_objet' => $id_objet
				));
			}
		}

		if ($texte) {
			if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
				$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
			else
				$flux['data'] .= $texte;
		}

	}


	return $flux;
}


/**
 * Remplir si besoin les infos de clientèle d'une commande avec les champs en dur
 * si les adresses livraison/facturation sont renseignées en dur dans la table des commandes
 * ce sont les adresses définitives et elles prennent le pas sur les adresses liées via coordonnée
 * (si on utilise pas ces champs en dur ça ne fait donc rien)
 *
 * @param array $flux
 * @return array
 */
function livraison_commande_infos_client($flux) {
	// si livraison_nom ou livraison_societe OU facturation_nom ou facturation_societe
	// est renseigné dans la commande c'est qu'on a fixé en dur dans la commandes
	// les adresses livraison et/ou facturation -> on prends la main sur les éventuelles infos renseignées en amont
	$commande = &$flux['args']['commande'];
	if (
		!empty($commande["livraison_nom"]) or !empty($commande["livraison_societe"])
		or !empty($commande["facturation_nom"]) or !empty($commande["facturation_societe"])
	) {
		// on reset les adresses éventuellement déjà renseignées qui sont caduques
		$flux['data']['livraison'] = [];
		$flux['data']['facturation'] = [];
		// Pour chacune des deux adresses
		foreach (['livraison', 'facturation'] as $type) {
			if (!empty($commande["${type}_nom"]) or !empty($commande["${type}_societe"])) {
				$adresse = $commande["${type}_adresse"];
				$adresse = explode("\n", $adresse,2);
				$voie = array_shift($adresse);
				$complement = (empty($adresse) ? '' : array_shift($adresse));
				$flux['data'][$type] = [
					'nom' => $commande["${type}_nom"],
					'organisation' => $commande["${type}_societe"],
					'adresse' => [
						'voie' => $voie,
						'complement' => $complement,
						'code_postal' => $commande["${type}_adresse_cp"],
						'ville' => $commande["${type}_adresse_ville"],
						'localite_dependante' => '',
						'zone_administrative' => '',
						'pays' => $commande["${type}_adresse_pays"],
					],
					'telephone' => $commande["${type}_telephone"],
				];
				if ($type === 'facturation') {
					$flux['data']['facturation']['no_tva_intra'] = $commande['facturation_no_tva_intra'];
				}
			}
		}
		// si l'une des 2 adresses est vide c'est géré par le plugin commandes au retour du pipeline qui remplira en fonction de l'autre
	}

	return $flux;
}

/**
 * Renseigner les infos de facturation liee a une commande
 * @param array $flux
 * @return array mixed
 */
function livraison_bank_dsp2_renseigner_facturation($flux) {
	if (
		isset($flux['args']['id_transaction'])
		and $id_transaction = intval($flux['args']['id_transaction'])
		and isset($flux['args']['id_commande'])
		and $id_commande = intval($flux['args']['id_commande'])
	) {
		$commande_infos_client = charger_fonction('infos_client', 'commande');
		$client = $commande_infos_client($id_commande);
		if (!empty($client['nom'])) {
			$flux['data']['nom'] = $client['nom'];
		}
		if (!empty($client['facturation'])) {
			if (!empty($client['facturation']['nom'])) {
				$flux['data']['nom'] = $client['facturation']['nom'];
			}
			$organisation = $client["facturation"]['organisation'];
			if ($organisation and empty($flux['data']['nom'])) {
				$flux['data']['nom'] = $organisation;
				$organisation = '';
			}
			if (!empty($client['facturation']['adresse'])) {
				$adresse = [$organisation];
				$adresse[] = (empty($client['facturation']['adresse']['voie']) ? '' : $client['facturation']['adresse']['voie']);
				$adresse[] = (empty($client['facturation']['adresse']['complement']) ? '' : $client['facturation']['adresse']['complement']);
				$adresse = array_filter($adresse);
				$flux['data']['adresse'] = trim(implode("\n", $adresse));
				$flux['data']['code_postal'] = (empty($client['facturation']['adresse']['code_postal']) ? '' : $client['facturation']['adresse']['code_postal']);
				$flux['data']['ville'] = (empty($client['facturation']['adresse']['ville']) ? '' : $client['facturation']['adresse']['ville']);
				$flux['data']['pays'] = (empty($client['facturation']['adresse']['pays']) ? '' : $client['facturation']['adresse']['pays']);
			}
		}
	}
	return $flux;
}
