<?php
/**
 * Fonctions utiles au plugin Livraison
 *
 * @plugin     Livraison
 * @copyright  2015
 * @author     Cédric
 * @licence    GNU/GPL
 * @package    SPIP\Livraison\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

function filtre_commande_livraison_necessaire_dist($id_commande){
	include_spip('inc/livraison');
	return (commande_livraison_necessaire($id_commande)?' ':'');
}

/**
 * Lister les modes de livraison possibles pour un panier et le cout associé
 * @param int $id_panier
 * @param string $pays
 * @param string $code_postal
 * @return array
 */
function panier_lister_livraisons_possibles($id_panier, $pays, $code_postal) {
	include_spip('inc/livraison');

	$details = sql_allfetsel("*","spip_paniers_liens","id_panier=".intval($id_panier));
	// il faut un id_detail sur chaque ligne
	foreach ($details as $k => $detail) {
		$details[$k]['id_detail'] = $k+1;
	}

	$choix_livraison = commande_trouver_livraisons_possibles($details, $pays, $code_postal);
	return $choix_livraison;
}


/**
 * Estimer le cout de livraison d'un panier en retenant le moins cher parmi les modes possibles
 * @param int $id_panier
 * @param string $pays
 * @param string $code_postal
 * @param bool $hors_taxe
 *
 * @return null|float
 */
function panier_estimer_cout_livraison($id_panier, $pays, $code_postal, $hors_taxe = false) {
	$cout = null;

	if ($choix_livraison = panier_lister_livraisons_possibles($id_panier, $pays, $code_postal)) {

		$champ_prix = ($hors_taxe ? 'prix_ht' : 'prix_ttc');
		foreach ($choix_livraison as $choix) {
			if (is_null($cout) or $choix[$champ_prix] < $cout) {
				$cout = $choix[$champ_prix];
			}
		}
	}

	return $cout;
}

function livraison_lister_details_bordereau($id_bordereaulivraison, $details = null) {
	if (is_null($details)) {
		$details = sql_getfetsel('details', 'spip_bordereaulivraisons', 'id_bordereaulivraison='.intval($id_bordereaulivraison));
	}
	$id_details = is_array($details) ? $details : array();
	if (!is_array($details)
		and strlen($details)
		and $ids = json_decode($details, true)) {
		$id_details = $ids;
	}
	return $id_details;
}
